
#include <windows.h>
#include <iostream>
#include <sstream>
#include <Psapi.h>
#pragma comment(lib, "psapi.lib") 


unsigned long getInt( std::string intAsString )
{

	unsigned int asInt = 0;
	std::stringstream ss;
	ss<<intAsString;
	ss>>asInt; 
	return asInt;
}
int main( int argc, char** argv )
{
	unsigned long pid;
	// write a program which does malloc of 4K bytes and does not do free, and use it's PID
	// program input must be ./a.exe PID, unnecessary error handling is not done here
	pid = getInt( argv[1] );
	HANDLE handle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid);
	if( NULL == handle ) 
	{
		std::cout << "not Able to Get process handle with PID=" <<  pid << std::endl;
		exit(1);
	}
	while(1)
	{
		Sleep(1000);
		PROCESS_MEMORY_COUNTERS pmc;
		GetProcessMemoryInfo(handle,&pmc,sizeof(pmc));
		
		std::cout << pmc.WorkingSetSize << std::endl;//Working bytes
		std::cout << pmc.PagefileUsage/1024  << std::endl;//private bytes
	}
}