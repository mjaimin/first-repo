#include <boost/thread.hpp>
#include <windows.h>
const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push, 8)
typedef struct THREADNAME_INFO {
    DWORD dwType; // Must be 0x1000.
    LPCSTR szName; // Pointer to name (in user addr space).
    DWORD dwThreadID; // Thread ID (-1=caller thread).
    DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)

void _SetThreadName(DWORD threadId, const char* threadName) {
    THREADNAME_INFO info;
    info.dwType = 0x1000;
    info.szName = threadName;
    info.dwThreadID = threadId;
    info.dwFlags = 0;
    __try {
        RaiseException( MS_VC_EXCEPTION, 0, sizeof(info)/sizeof(ULONG_PTR), (ULONG_PTR*)&info );
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
    }
}
void SetThreadName(boost::thread::id threadId, std::string threadName) {
    // convert string to char*
    const char* cchar = threadName.c_str();
    // convert HEX string to DWORD
    unsigned int dwThreadId;
    std::stringstream ss;
    ss << std::hex << threadId;
    ss >> dwThreadId;
    // set thread name
    _SetThreadName((DWORD)dwThreadId, cchar);
}

class SampleClass
{   
    boost::thread threadHandle;
    void sampleThreadFunc();
public:
    void threadLauncherFunc();
    void joinFunc();
};

void SampleClass::threadLauncherFunc()
{
    threadHandle = boost::thread(&SampleClass::sampleThreadFunc,this);
}

void SampleClass::sampleThreadFunc(void)
{
    //This is useful when you debug threads in visual studio and you
    //want to chose one thread from thread list of debugger
    SetThreadName(boost::this_thread::get_id(),"SomeNameOfThread");
    while(1);
}
void SampleClass::joinFunc(void)
{
    threadHandle.join();
}

int main()
{
    SampleClass obj;
    obj.threadLauncherFunc();
    obj.joinFunc();
}