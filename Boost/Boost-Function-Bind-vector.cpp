//Pushing different class Objects in to a vector
#include <vector>
#include <iostream>
#include <boost/function.hpp>
#include <boost/bind.hpp>
class Sphere
{
public:
    void Draw()
    {
        std::cout << "From Sphere" << std::endl;
    }
};
class Plane
{
public:
    void Draw()
    {
        std::cout << "From Plane" << std::endl;
    }
};
int main()
{
    std::vector<boost::function<void()>> drawings;
    Sphere s;
    Plane p;
    drawings.push_back(boost::bind(&Sphere::Draw,&s));
    drawings.push_back(boost::bind(&Plane::Draw,&p));
    for(std::vector<boost::function<void()>>::iterator i = drawings.begin(); i != drawings.end(); ++i) (*i)();
}